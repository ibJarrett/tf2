package me.chaseoes.tf2.events;

import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class TF2DeathEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    public Player player;
    public Player killer;

    public TF2DeathEvent(Player p, Player k) {
        player = p;
        killer = k;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    
    
    
    public Player getKiller() {
        return killer;
    }

}
